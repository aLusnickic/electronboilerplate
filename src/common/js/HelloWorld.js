"use strict";

class HelloWorld {
  constructor() {
    var div = document.createElement("div");
    div.className = "helloWorld";
    div.innerHTML = "Hello World";
    document.body.appendChild(div);
  }
}

module.exports = HelloWorld;
