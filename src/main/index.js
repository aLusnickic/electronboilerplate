"use stric";

const electron = require("electron");
const { BrowserWindow } = require("electron");
const app = electron.app;
const ipcMain = electron.ipcMain; // Used for communication between main and renderer process

app.on("ready", createWindow);

var mainWindow = undefined;

var userDataPath = app.getPath("userData"); // path to the installed electron app
var isDevelopment = process.env.NODE_ENV != "production"; // development mode is run using 'yarn dev'

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1040,
    height: 650,
    title: "TOI Editor",
    webPreferences: {
      webSecurity: true,
      nodeIntegration: true
    }
  });
  mainWindow.on("close", () => {
    mainWindow = null;
  });
  mainWindow.webContents.openDevTools();

  if (isDevelopment) {
    mainWindow.loadURL(
      `http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`
    );
  } else {
    mainWindow.loadURL(`file://${__dirname}/index.html`);
  }
}
