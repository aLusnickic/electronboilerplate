var path = require("path");
var os = require('os');

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, "dist/"),
        publicPath: "/"
    },
    resolve: {
        modules: [
            'node_modules',
            'src'
        ]
    },
    module: {
        rules: [{
                test: /\.html$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        interpolate: true
                    }
                }
            },
            {
                test: /\.css$/,
                use: ['css-loader'],
            },
            {
                test: /favicon\.ico$/,
                loader: 'url',
                query: {
                    limit: 1,
                    name: '[name].[ext]',
                },
            }
        ]
    }
}